EESchema Schematic File Version 4
LIBS:accent-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L accent:ProMicro U1
U 1 1 5C96007C
P 2250 2150
F 0 "U1" H 2250 2965 50  0000 C CNN
F 1 "ProMicro" H 2250 2874 50  0000 C CNN
F 2 "accent-footprint:ProMicro" H 2200 2250 50  0001 C CNN
F 3 "" H 2200 2250 50  0001 C CNN
	1    2250 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D1
U 1 1 5C960268
P 5950 1700
F 0 "D1" V 5996 1621 50  0000 R CNN
F 1 "D" V 5905 1621 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 5950 1700 50  0001 C CNN
F 3 "~" H 5950 1700 50  0001 C CNN
	1    5950 1700
	0    -1   -1   0   
$EndComp
Text GLabel 5350 1150 1    50   Input ~ 0
col6
$Comp
L accent:SW_PUSH SW1
U 1 1 5C985F82
P 5650 1550
F 0 "SW1" H 5650 1895 50  0000 C CNN
F 1 "SW_PUSH" H 5650 1804 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 5650 1713 20  0000 C CNN
F 3 "" H 5650 1550 50  0000 C CNN
	1    5650 1550
	1    0    0    -1  
$EndComp
Text GLabel 4750 1850 0    50   Input ~ 0
row0
Wire Wire Line
	5950 1850 6750 1850
$Comp
L Device:D D2
U 1 1 5C9821B9
P 6750 1700
F 0 "D2" V 6796 1621 50  0000 R CNN
F 1 "D" V 6705 1621 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 6750 1700 50  0001 C CNN
F 3 "~" H 6750 1700 50  0001 C CNN
	1    6750 1700
	0    -1   -1   0   
$EndComp
Text GLabel 6150 1150 1    50   Input ~ 0
col5
$Comp
L accent:SW_PUSH SW2
U 1 1 5C9821C0
P 6450 1550
F 0 "SW2" H 6450 1895 50  0000 C CNN
F 1 "SW_PUSH" H 6450 1804 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 6450 1713 20  0000 C CNN
F 3 "" H 6450 1550 50  0000 C CNN
	1    6450 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1550 6150 1150
$Comp
L Device:D D3
U 1 1 5C982287
P 7550 1700
F 0 "D3" V 7596 1621 50  0000 R CNN
F 1 "D" V 7505 1621 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 7550 1700 50  0001 C CNN
F 3 "~" H 7550 1700 50  0001 C CNN
	1    7550 1700
	0    -1   -1   0   
$EndComp
Text GLabel 6950 1150 1    50   Input ~ 0
col4
$Comp
L accent:SW_PUSH SW3
U 1 1 5C98228F
P 7250 1550
F 0 "SW3" H 7250 1895 50  0000 C CNN
F 1 "SW_PUSH" H 7250 1804 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 7250 1713 20  0000 C CNN
F 3 "" H 7250 1550 50  0000 C CNN
	1    7250 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 1550 6950 1150
$Comp
L Device:D D4
U 1 1 5C9822D0
P 8350 1700
F 0 "D4" V 8396 1621 50  0000 R CNN
F 1 "D" V 8305 1621 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 8350 1700 50  0001 C CNN
F 3 "~" H 8350 1700 50  0001 C CNN
	1    8350 1700
	0    -1   -1   0   
$EndComp
Text GLabel 7750 1150 1    50   Input ~ 0
col3
$Comp
L accent:SW_PUSH SW4
U 1 1 5C9822D8
P 8050 1550
F 0 "SW4" H 8050 1895 50  0000 C CNN
F 1 "SW_PUSH" H 8050 1804 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 8050 1713 20  0000 C CNN
F 3 "" H 8050 1550 50  0000 C CNN
	1    8050 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1550 7750 1150
Connection ~ 6750 1850
Wire Wire Line
	6750 1850 7550 1850
Connection ~ 7550 1850
Wire Wire Line
	7550 1850 8350 1850
$Comp
L Device:D D5
U 1 1 5C9826D7
P 9150 1700
F 0 "D5" V 9196 1621 50  0000 R CNN
F 1 "D" V 9105 1621 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 9150 1700 50  0001 C CNN
F 3 "~" H 9150 1700 50  0001 C CNN
	1    9150 1700
	0    -1   -1   0   
$EndComp
Text GLabel 8550 1150 1    50   Input ~ 0
col2
$Comp
L accent:SW_PUSH SW5
U 1 1 5C9826DE
P 8850 1550
F 0 "SW5" H 8850 1895 50  0000 C CNN
F 1 "SW_PUSH" H 8850 1804 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 8850 1713 20  0000 C CNN
F 3 "" H 8850 1550 50  0000 C CNN
	1    8850 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 1550 8550 1150
Connection ~ 8350 1850
Connection ~ 9150 1850
Wire Wire Line
	8350 1850 9150 1850
Wire Wire Line
	9150 1850 9950 1850
$Comp
L Device:D D6
U 1 1 5C982842
P 9950 1700
F 0 "D6" V 9996 1621 50  0000 R CNN
F 1 "D" V 9905 1621 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 9950 1700 50  0001 C CNN
F 3 "~" H 9950 1700 50  0001 C CNN
	1    9950 1700
	0    -1   -1   0   
$EndComp
Text GLabel 9350 1150 1    50   Input ~ 0
col1
$Comp
L accent:SW_PUSH SW6
U 1 1 5C982849
P 9650 1550
F 0 "SW6" H 9650 1895 50  0000 C CNN
F 1 "SW_PUSH" H 9650 1804 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 9650 1713 20  0000 C CNN
F 3 "" H 9650 1550 50  0000 C CNN
	1    9650 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 1550 9350 1150
$Comp
L Device:D D7
U 1 1 5C982B10
P 5950 2500
F 0 "D7" V 5996 2421 50  0000 R CNN
F 1 "D" V 5905 2421 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 5950 2500 50  0001 C CNN
F 3 "~" H 5950 2500 50  0001 C CNN
	1    5950 2500
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW7
U 1 1 5C982B16
P 5650 2350
F 0 "SW7" H 5650 2695 50  0000 C CNN
F 1 "SW_PUSH" H 5650 2604 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 5650 2513 20  0000 C CNN
F 3 "" H 5650 2350 50  0000 C CNN
	1    5650 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:D D8
U 1 1 5C982B1D
P 6750 2500
F 0 "D8" V 6796 2421 50  0000 R CNN
F 1 "D" V 6705 2421 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 6750 2500 50  0001 C CNN
F 3 "~" H 6750 2500 50  0001 C CNN
	1    6750 2500
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW8
U 1 1 5C982B23
P 6450 2350
F 0 "SW8" H 6450 2695 50  0000 C CNN
F 1 "SW_PUSH" H 6450 2604 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 6450 2513 20  0000 C CNN
F 3 "" H 6450 2350 50  0000 C CNN
	1    6450 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:D D9
U 1 1 5C982B2A
P 7550 2500
F 0 "D9" V 7596 2421 50  0000 R CNN
F 1 "D" V 7505 2421 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 7550 2500 50  0001 C CNN
F 3 "~" H 7550 2500 50  0001 C CNN
	1    7550 2500
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW9
U 1 1 5C982B30
P 7250 2350
F 0 "SW9" H 7250 2695 50  0000 C CNN
F 1 "SW_PUSH" H 7250 2604 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 7250 2513 20  0000 C CNN
F 3 "" H 7250 2350 50  0000 C CNN
	1    7250 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:D D10
U 1 1 5C982B37
P 8350 2500
F 0 "D10" V 8396 2421 50  0000 R CNN
F 1 "D" V 8305 2421 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 8350 2500 50  0001 C CNN
F 3 "~" H 8350 2500 50  0001 C CNN
	1    8350 2500
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW10
U 1 1 5C982B3D
P 8050 2350
F 0 "SW10" H 8050 2695 50  0000 C CNN
F 1 "SW_PUSH" H 8050 2604 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 8050 2513 20  0000 C CNN
F 3 "" H 8050 2350 50  0000 C CNN
	1    8050 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:D D11
U 1 1 5C982B44
P 9150 2500
F 0 "D11" V 9196 2421 50  0000 R CNN
F 1 "D" V 9105 2421 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 9150 2500 50  0001 C CNN
F 3 "~" H 9150 2500 50  0001 C CNN
	1    9150 2500
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW11
U 1 1 5C982B4A
P 8850 2350
F 0 "SW11" H 8850 2695 50  0000 C CNN
F 1 "SW_PUSH" H 8850 2604 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 8850 2513 20  0000 C CNN
F 3 "" H 8850 2350 50  0000 C CNN
	1    8850 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:D D12
U 1 1 5C982B51
P 9950 2500
F 0 "D12" V 9996 2421 50  0000 R CNN
F 1 "D" V 9905 2421 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 9950 2500 50  0001 C CNN
F 3 "~" H 9950 2500 50  0001 C CNN
	1    9950 2500
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW12
U 1 1 5C982B57
P 9650 2350
F 0 "SW12" H 9650 2695 50  0000 C CNN
F 1 "SW_PUSH" H 9650 2604 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 9650 2513 20  0000 C CNN
F 3 "" H 9650 2350 50  0000 C CNN
	1    9650 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:D D13
U 1 1 5C982EB7
P 5950 3300
F 0 "D13" V 5996 3221 50  0000 R CNN
F 1 "D" V 5905 3221 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 5950 3300 50  0001 C CNN
F 3 "~" H 5950 3300 50  0001 C CNN
	1    5950 3300
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW13
U 1 1 5C982EBD
P 5650 3150
F 0 "SW13" H 5650 3495 50  0000 C CNN
F 1 "SW_PUSH" H 5650 3404 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 5650 3313 20  0000 C CNN
F 3 "" H 5650 3150 50  0000 C CNN
	1    5650 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D14
U 1 1 5C982EC4
P 6750 3300
F 0 "D14" V 6796 3221 50  0000 R CNN
F 1 "D" V 6705 3221 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 6750 3300 50  0001 C CNN
F 3 "~" H 6750 3300 50  0001 C CNN
	1    6750 3300
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW14
U 1 1 5C982ECA
P 6450 3150
F 0 "SW14" H 6450 3495 50  0000 C CNN
F 1 "SW_PUSH" H 6450 3404 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 6450 3313 20  0000 C CNN
F 3 "" H 6450 3150 50  0000 C CNN
	1    6450 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D15
U 1 1 5C982ED1
P 7550 3300
F 0 "D15" V 7596 3221 50  0000 R CNN
F 1 "D" V 7505 3221 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 7550 3300 50  0001 C CNN
F 3 "~" H 7550 3300 50  0001 C CNN
	1    7550 3300
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW15
U 1 1 5C982ED7
P 7250 3150
F 0 "SW15" H 7250 3495 50  0000 C CNN
F 1 "SW_PUSH" H 7250 3404 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 7250 3313 20  0000 C CNN
F 3 "" H 7250 3150 50  0000 C CNN
	1    7250 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D16
U 1 1 5C982EDE
P 8350 3300
F 0 "D16" V 8396 3221 50  0000 R CNN
F 1 "D" V 8305 3221 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 8350 3300 50  0001 C CNN
F 3 "~" H 8350 3300 50  0001 C CNN
	1    8350 3300
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW16
U 1 1 5C982EE4
P 8050 3150
F 0 "SW16" H 8050 3495 50  0000 C CNN
F 1 "SW_PUSH" H 8050 3404 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 8050 3313 20  0000 C CNN
F 3 "" H 8050 3150 50  0000 C CNN
	1    8050 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D17
U 1 1 5C982EEB
P 9150 3300
F 0 "D17" V 9196 3221 50  0000 R CNN
F 1 "D" V 9105 3221 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 9150 3300 50  0001 C CNN
F 3 "~" H 9150 3300 50  0001 C CNN
	1    9150 3300
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW17
U 1 1 5C982EF1
P 8850 3150
F 0 "SW17" H 8850 3495 50  0000 C CNN
F 1 "SW_PUSH" H 8850 3404 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 8850 3313 20  0000 C CNN
F 3 "" H 8850 3150 50  0000 C CNN
	1    8850 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D18
U 1 1 5C982EF8
P 9950 3300
F 0 "D18" V 9996 3221 50  0000 R CNN
F 1 "D" V 9905 3221 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 9950 3300 50  0001 C CNN
F 3 "~" H 9950 3300 50  0001 C CNN
	1    9950 3300
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW18
U 1 1 5C982EFE
P 9650 3150
F 0 "SW18" H 9650 3495 50  0000 C CNN
F 1 "SW_PUSH" H 9650 3404 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 9650 3313 20  0000 C CNN
F 3 "" H 9650 3150 50  0000 C CNN
	1    9650 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:D D19
U 1 1 5C98335C
P 5950 4100
F 0 "D19" V 5996 4021 50  0000 R CNN
F 1 "D" V 5905 4021 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 5950 4100 50  0001 C CNN
F 3 "~" H 5950 4100 50  0001 C CNN
	1    5950 4100
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW19
U 1 1 5C983362
P 5650 3950
F 0 "SW19" H 5650 4295 50  0000 C CNN
F 1 "SW_PUSH" H 5650 4204 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 5650 4113 20  0000 C CNN
F 3 "" H 5650 3950 50  0000 C CNN
	1    5650 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D20
U 1 1 5C983369
P 6750 4100
F 0 "D20" V 6796 4021 50  0000 R CNN
F 1 "D" V 6705 4021 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 6750 4100 50  0001 C CNN
F 3 "~" H 6750 4100 50  0001 C CNN
	1    6750 4100
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW20
U 1 1 5C98336F
P 6450 3950
F 0 "SW20" H 6450 4295 50  0000 C CNN
F 1 "SW_PUSH" H 6450 4204 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 6450 4113 20  0000 C CNN
F 3 "" H 6450 3950 50  0000 C CNN
	1    6450 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D21
U 1 1 5C983376
P 7550 4100
F 0 "D21" V 7596 4021 50  0000 R CNN
F 1 "D" V 7505 4021 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 7550 4100 50  0001 C CNN
F 3 "~" H 7550 4100 50  0001 C CNN
	1    7550 4100
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW21
U 1 1 5C98337C
P 7250 3950
F 0 "SW21" H 7250 4295 50  0000 C CNN
F 1 "SW_PUSH" H 7250 4204 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 7250 4113 20  0000 C CNN
F 3 "" H 7250 3950 50  0000 C CNN
	1    7250 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D22
U 1 1 5C983383
P 8350 4100
F 0 "D22" V 8396 4021 50  0000 R CNN
F 1 "D" V 8305 4021 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 8350 4100 50  0001 C CNN
F 3 "~" H 8350 4100 50  0001 C CNN
	1    8350 4100
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW22
U 1 1 5C983389
P 8050 3950
F 0 "SW22" H 8050 4295 50  0000 C CNN
F 1 "SW_PUSH" H 8050 4204 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 8050 4113 20  0000 C CNN
F 3 "" H 8050 3950 50  0000 C CNN
	1    8050 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D23
U 1 1 5C983390
P 9150 4100
F 0 "D23" V 9196 4021 50  0000 R CNN
F 1 "D" V 9105 4021 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 9150 4100 50  0001 C CNN
F 3 "~" H 9150 4100 50  0001 C CNN
	1    9150 4100
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW23
U 1 1 5C983396
P 8850 3950
F 0 "SW23" H 8850 4295 50  0000 C CNN
F 1 "SW_PUSH" H 8850 4204 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 8850 4113 20  0000 C CNN
F 3 "" H 8850 3950 50  0000 C CNN
	1    8850 3950
	1    0    0    -1  
$EndComp
Text GLabel 4750 2650 0    50   Input ~ 0
row1
Text GLabel 4750 3450 0    50   Input ~ 0
row2
Text GLabel 4750 4250 0    50   Input ~ 0
row3
Wire Wire Line
	5950 2650 6750 2650
Connection ~ 6750 2650
Wire Wire Line
	6750 2650 7550 2650
Connection ~ 7550 2650
Wire Wire Line
	7550 2650 8350 2650
Connection ~ 8350 2650
Wire Wire Line
	8350 2650 9150 2650
Connection ~ 9150 2650
Wire Wire Line
	9150 2650 9950 2650
Wire Wire Line
	5950 3450 6750 3450
Connection ~ 6750 3450
Wire Wire Line
	6750 3450 7550 3450
Connection ~ 7550 3450
Wire Wire Line
	7550 3450 8350 3450
Connection ~ 8350 3450
Wire Wire Line
	8350 3450 9150 3450
Connection ~ 9150 3450
Wire Wire Line
	9150 3450 9950 3450
Connection ~ 6750 4250
Connection ~ 7550 4250
Connection ~ 8350 4250
Wire Wire Line
	6150 1550 6150 2350
Connection ~ 6150 1550
Connection ~ 6150 2350
Wire Wire Line
	6150 2350 6150 3150
Connection ~ 6150 3150
Wire Wire Line
	6150 3150 6150 3950
Wire Wire Line
	6950 1550 6950 2350
Connection ~ 6950 1550
Connection ~ 6950 2350
Wire Wire Line
	6950 2350 6950 3150
Connection ~ 6950 3150
Wire Wire Line
	6950 3150 6950 3950
Wire Wire Line
	7750 1550 7750 2350
Connection ~ 7750 1550
Connection ~ 7750 2350
Wire Wire Line
	7750 2350 7750 3150
Connection ~ 7750 3150
Wire Wire Line
	7750 3150 7750 3950
Wire Wire Line
	8550 1550 8550 2350
Connection ~ 8550 1550
Connection ~ 8550 2350
Wire Wire Line
	8550 2350 8550 3150
Connection ~ 8550 3150
Wire Wire Line
	8550 3150 8550 3950
Wire Wire Line
	9350 1550 9350 2350
Connection ~ 9350 1550
Connection ~ 9350 2350
Wire Wire Line
	9350 2350 9350 3150
Wire Wire Line
	4750 1850 5950 1850
Connection ~ 5950 1850
Wire Wire Line
	4750 2650 5950 2650
Connection ~ 5950 2650
Wire Wire Line
	4750 3450 5950 3450
Connection ~ 5950 3450
Connection ~ 5950 4250
Wire Wire Line
	5350 1150 5350 1550
Connection ~ 5350 1550
Wire Wire Line
	5350 1550 5350 2350
Connection ~ 5350 2350
Wire Wire Line
	5350 2350 5350 3150
Connection ~ 5350 3150
Wire Wire Line
	5350 3150 5350 3950
Wire Wire Line
	8350 4250 9150 4250
Wire Wire Line
	7550 4250 8350 4250
Wire Wire Line
	6750 4250 7550 4250
Wire Wire Line
	5950 4250 6750 4250
Wire Wire Line
	4750 4250 5950 4250
Text GLabel 1600 2400 0    50   Input ~ 0
row0
Text GLabel 1600 2500 0    50   Input ~ 0
row1
Text GLabel 1600 2600 0    50   Input ~ 0
row2
Text GLabel 1600 2700 0    50   Input ~ 0
row3
Text GLabel 2900 2100 2    50   Input ~ 0
col0
Text GLabel 2900 2300 2    50   Input ~ 0
col2
Text GLabel 2900 2200 2    50   Input ~ 0
col1
Text GLabel 2900 2400 2    50   Input ~ 0
col3
Text GLabel 2900 2500 2    50   Input ~ 0
col4
Text GLabel 2900 2600 2    50   Input ~ 0
col5
NoConn ~ 1600 2300
NoConn ~ 1600 2200
$Comp
L Device:D D24
U 1 1 5C98A60B
P 9950 4100
F 0 "D24" V 9996 4021 50  0000 R CNN
F 1 "D" V 9905 4021 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 9950 4100 50  0001 C CNN
F 3 "~" H 9950 4100 50  0001 C CNN
	1    9950 4100
	0    -1   -1   0   
$EndComp
$Comp
L accent:SW_PUSH SW24
U 1 1 5C98A612
P 9650 3950
F 0 "SW24" H 9650 4295 50  0000 C CNN
F 1 "SW_PUSH" H 9650 4204 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 9650 4113 20  0000 C CNN
F 3 "" H 9650 3950 50  0000 C CNN
	1    9650 3950
	1    0    0    -1  
$EndComp
Text GLabel 10150 1150 1    50   Input ~ 0
col0
Wire Wire Line
	10150 1150 10150 3950
Wire Wire Line
	9350 3150 9350 3950
Connection ~ 9350 3150
Wire Wire Line
	9150 4250 9950 4250
Connection ~ 9150 4250
$Comp
L accent:VCC #PWR0101
U 1 1 5C999BD4
P 2050 3400
F 0 "#PWR0101" H 2050 3250 50  0001 C CNN
F 1 "VCC" H 2067 3573 50  0000 C CNN
F 2 "" H 2050 3400 50  0000 C CNN
F 3 "" H 2050 3400 50  0000 C CNN
	1    2050 3400
	1    0    0    -1  
$EndComp
$Comp
L accent:GND #PWR0102
U 1 1 5C999CA0
P 2050 4100
F 0 "#PWR0102" H 2050 3850 50  0001 C CNN
F 1 "GND" H 2055 3927 50  0000 C CNN
F 2 "" H 2050 4100 50  0000 C CNN
F 3 "" H 2050 4100 50  0000 C CNN
	1    2050 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 3750 2050 3400
Wire Wire Line
	1850 3850 2050 3850
Wire Wire Line
	2050 3850 2050 4100
$Comp
L accent:i2c_pin P2
U 1 1 5C984B83
P 2350 4150
F 0 "P2" V 2223 4230 50  0000 L CNN
F 1 "i2c_pin" V 2314 4230 50  0000 L CNN
F 2 "accent-footprint:1pin_conn" H 2350 4150 50  0001 C CNN
F 3 "" H 2350 4150 50  0001 C CNN
	1    2350 4150
	0    1    1    0   
$EndComp
$Comp
L accent:i2c_pin P1
U 1 1 5C984C63
P 2350 3400
F 0 "P1" V 2316 3312 50  0000 R CNN
F 1 "i2c_pin" V 2225 3312 50  0000 R CNN
F 2 "accent-footprint:1pin_conn" H 2350 3400 50  0001 C CNN
F 3 "" H 2350 3400 50  0001 C CNN
	1    2350 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1850 3750 2050 3750
$Comp
L accent:MJ-4PP-9 J1
U 1 1 5C993675
P 1400 3750
F 0 "J1" H 1406 4147 60  0000 C CNN
F 1 "MJ-4PP-9" H 1406 4041 60  0000 C CNN
F 2 "accent-footprint:MJ-4PP-9" H 1400 3750 60  0001 C CNN
F 3 "" H 1400 3750 60  0000 C CNN
	1    1400 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3600 2350 3600
Wire Wire Line
	1850 3950 2350 3950
$Comp
L accent:Jumper_jack JPJ1
U 1 1 5C98F8A4
P 2700 3950
F 0 "JPJ1" V 2889 3950 50  0000 C CNN
F 1 "Jumper_jack" V 2805 3950 40  0000 C CNN
F 2 "accent-footprint:jumper_jack" H 2700 3950 60  0001 C CNN
F 3 "" H 2700 3950 60  0000 C CNN
	1    2700 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 3950 2350 3950
Connection ~ 2350 3950
Text GLabel 2900 3950 2    50   Input ~ 0
DATA
$Comp
L accent:OLED J2
U 1 1 5C9929F9
P 900 5950
F 0 "J2" H 819 5575 50  0000 C CNN
F 1 "OLED" H 819 5666 50  0000 C CNN
F 2 "accent-footprint:OLED" H 900 5950 50  0001 C CNN
F 3 "" H 900 5950 50  0000 C CNN
	1    900  5950
	-1   0    0    1   
$EndComp
Wire Wire Line
	1100 6100 1200 6100
Wire Wire Line
	1100 6000 1300 6000
Wire Wire Line
	1100 5900 1400 5900
Wire Wire Line
	1100 5800 1500 5800
$Comp
L accent:Jumper_oled JP1
U 1 1 5C9A5859
P 1900 4700
F 0 "JP1" V 2089 4700 50  0000 C CNN
F 1 "Jumper_oled" V 2005 4700 40  0000 C CNN
F 2 "accent-footprint:Jumper_oled" H 1900 4700 60  0001 C CNN
F 3 "" H 1900 4700 60  0000 C CNN
	1    1900 4700
	0    -1   -1   0   
$EndComp
$Comp
L accent:Jumper_oled JP2
U 1 1 5C9A73EE
P 1900 5000
F 0 "JP2" V 2089 5000 50  0000 C CNN
F 1 "Jumper_oled" V 2005 5000 40  0000 C CNN
F 2 "accent-footprint:Jumper_oled" H 1900 5000 60  0001 C CNN
F 3 "" H 1900 5000 60  0000 C CNN
	1    1900 5000
	0    -1   -1   0   
$EndComp
$Comp
L accent:Jumper_oled JP3
U 1 1 5C9A8E88
P 1900 5300
F 0 "JP3" V 2089 5300 50  0000 C CNN
F 1 "Jumper_oled" V 2005 5300 40  0000 C CNN
F 2 "accent-footprint:Jumper_oled" H 1900 5300 60  0001 C CNN
F 3 "" H 1900 5300 60  0000 C CNN
	1    1900 5300
	0    -1   -1   0   
$EndComp
$Comp
L accent:Jumper_oled JP4
U 1 1 5C9AA8B2
P 1900 5600
F 0 "JP4" V 2089 5600 50  0000 C CNN
F 1 "Jumper_oled" V 2005 5600 40  0000 C CNN
F 2 "accent-footprint:Jumper_oled" H 1900 5600 60  0001 C CNN
F 3 "" H 1900 5600 60  0000 C CNN
	1    1900 5600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1500 4700 1700 4700
Wire Wire Line
	1500 4700 1500 5800
Wire Wire Line
	1700 5000 1400 5000
Wire Wire Line
	1400 5000 1400 5900
Wire Wire Line
	1700 5300 1300 5300
Wire Wire Line
	1300 5300 1300 6000
Wire Wire Line
	1200 5600 1700 5600
Wire Wire Line
	1200 5600 1200 6100
$Comp
L accent:Jumper_oled JP5
U 1 1 5C9C5C9E
P 1900 6250
F 0 "JP5" V 2089 6250 50  0000 C CNN
F 1 "Jumper_oled" V 2005 6250 40  0000 C CNN
F 2 "accent-footprint:Jumper_oled" H 1900 6250 60  0001 C CNN
F 3 "" H 1900 6250 60  0000 C CNN
	1    1900 6250
	0    -1   -1   0   
$EndComp
$Comp
L accent:Jumper_oled JP6
U 1 1 5C9C5CA4
P 1900 6550
F 0 "JP6" V 2089 6550 50  0000 C CNN
F 1 "Jumper_oled" V 2005 6550 40  0000 C CNN
F 2 "accent-footprint:Jumper_oled" H 1900 6550 60  0001 C CNN
F 3 "" H 1900 6550 60  0000 C CNN
	1    1900 6550
	0    -1   -1   0   
$EndComp
$Comp
L accent:Jumper_oled JP7
U 1 1 5C9C5CAA
P 1900 6850
F 0 "JP7" V 2089 6850 50  0000 C CNN
F 1 "Jumper_oled" V 2005 6850 40  0000 C CNN
F 2 "accent-footprint:Jumper_oled" H 1900 6850 60  0001 C CNN
F 3 "" H 1900 6850 60  0000 C CNN
	1    1900 6850
	0    -1   -1   0   
$EndComp
$Comp
L accent:Jumper_oled JP8
U 1 1 5C9C5CB0
P 1900 7150
F 0 "JP8" V 2089 7150 50  0000 C CNN
F 1 "Jumper_oled" V 2005 7150 40  0000 C CNN
F 2 "accent-footprint:Jumper_oled" H 1900 7150 60  0001 C CNN
F 3 "" H 1900 7150 60  0000 C CNN
	1    1900 7150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1500 5800 1500 6250
Wire Wire Line
	1500 6250 1700 6250
Connection ~ 1500 5800
Wire Wire Line
	1400 5900 1400 6550
Wire Wire Line
	1400 6550 1700 6550
Connection ~ 1400 5900
Wire Wire Line
	1300 6000 1300 6850
Wire Wire Line
	1300 6850 1700 6850
Connection ~ 1300 6000
Wire Wire Line
	1700 7150 1200 7150
Wire Wire Line
	1200 7150 1200 6100
Connection ~ 1200 6100
$Comp
L accent:GND #PWR0103
U 1 1 5C9CFF6B
P 2100 4700
F 0 "#PWR0103" H 2100 4450 50  0001 C CNN
F 1 "GND" V 2105 4572 50  0000 R CNN
F 2 "" H 2100 4700 50  0000 C CNN
F 3 "" H 2100 4700 50  0000 C CNN
	1    2100 4700
	0    -1   -1   0   
$EndComp
$Comp
L accent:GND #PWR0104
U 1 1 5C9D01B9
P 2100 7150
F 0 "#PWR0104" H 2100 6900 50  0001 C CNN
F 1 "GND" V 2105 7022 50  0000 R CNN
F 2 "" H 2100 7150 50  0000 C CNN
F 3 "" H 2100 7150 50  0000 C CNN
	1    2100 7150
	0    -1   -1   0   
$EndComp
$Comp
L accent:VCC #PWR0105
U 1 1 5C9D0379
P 2100 5000
F 0 "#PWR0105" H 2100 4850 50  0001 C CNN
F 1 "VCC" V 2117 5128 50  0000 L CNN
F 2 "" H 2100 5000 50  0000 C CNN
F 3 "" H 2100 5000 50  0000 C CNN
	1    2100 5000
	0    1    1    0   
$EndComp
$Comp
L accent:VCC #PWR0106
U 1 1 5C9D062B
P 2100 6850
F 0 "#PWR0106" H 2100 6700 50  0001 C CNN
F 1 "VCC" V 2117 6978 50  0000 L CNN
F 2 "" H 2100 6850 50  0000 C CNN
F 3 "" H 2100 6850 50  0000 C CNN
	1    2100 6850
	0    1    1    0   
$EndComp
Text GLabel 2100 5300 2    50   Input ~ 0
SCL
Text GLabel 2100 6550 2    50   Input ~ 0
SCL
Text GLabel 2100 5600 2    50   Input ~ 0
SDA
Text GLabel 2100 6250 2    50   Input ~ 0
SDA
$Comp
L accent:SW_RESET SWRT1
U 1 1 5C986361
P 3650 4850
F 0 "SWRT1" H 3650 5105 50  0000 C CNN
F 1 "SW_RESET" H 3650 5014 50  0000 C CNN
F 2 "accent-footprint:TACT_SWITCH_TVBP06" H 3650 4850 50  0001 C CNN
F 3 "" H 3650 4850 50  0000 C CNN
	1    3650 4850
	1    0    0    -1  
$EndComp
Text GLabel 3950 4850 2    50   Input ~ 0
RESET
$Comp
L accent:GND #PWR0107
U 1 1 5C986694
P 3350 4850
F 0 "#PWR0107" H 3350 4600 50  0001 C CNN
F 1 "GND" V 3355 4722 50  0000 R CNN
F 2 "" H 3350 4850 50  0000 C CNN
F 3 "" H 3350 4850 50  0000 C CNN
	1    3350 4850
	0    1    1    0   
$EndComp
$Comp
L accent:LED J3
U 1 1 5C98A57F
P 3200 5850
F 0 "J3" H 3280 6167 50  0000 C CNN
F 1 "LED" H 3280 6076 50  0000 C CNN
F 2 "accent-footprint:LED" H 3200 5850 50  0001 C CNN
F 3 "" H 3200 5850 50  0001 C CNN
	1    3200 5850
	1    0    0    -1  
$EndComp
$Comp
L accent:GND #PWR0108
U 1 1 5C98A68B
P 3400 5950
F 0 "#PWR0108" H 3400 5700 50  0001 C CNN
F 1 "GND" H 3405 5777 50  0000 C CNN
F 2 "" H 3400 5950 50  0000 C CNN
F 3 "" H 3400 5950 50  0000 C CNN
	1    3400 5950
	1    0    0    -1  
$EndComp
$Comp
L accent:VCC #PWR0109
U 1 1 5C98A781
P 3400 5750
F 0 "#PWR0109" H 3400 5600 50  0001 C CNN
F 1 "VCC" H 3417 5923 50  0000 C CNN
F 2 "" H 3400 5750 50  0000 C CNN
F 3 "" H 3400 5750 50  0000 C CNN
	1    3400 5750
	1    0    0    -1  
$EndComp
Text GLabel 3400 5850 2    50   Input ~ 0
LED
$Comp
L accent:RESISTOR R1
U 1 1 5C98CA8C
P 3250 6750
F 0 "R1" H 3320 6796 50  0000 L CNN
F 1 "R" H 3320 6705 50  0000 L CNN
F 2 "accent-footprint:RESISTOR" V 3180 6750 50  0001 C CNN
F 3 "" H 3250 6750 50  0001 C CNN
	1    3250 6750
	1    0    0    -1  
$EndComp
$Comp
L accent:VCC #PWR0110
U 1 1 5C98CF78
P 3250 6600
F 0 "#PWR0110" H 3250 6450 50  0001 C CNN
F 1 "VCC" H 3267 6773 50  0000 C CNN
F 2 "" H 3250 6600 50  0000 C CNN
F 3 "" H 3250 6600 50  0000 C CNN
	1    3250 6600
	1    0    0    -1  
$EndComp
Text GLabel 3250 6900 3    50   Input ~ 0
SDA
$Comp
L accent:RESISTOR R2
U 1 1 5C98F080
P 3650 6750
F 0 "R2" H 3720 6796 50  0000 L CNN
F 1 "R" H 3720 6705 50  0000 L CNN
F 2 "accent-footprint:RESISTOR" V 3580 6750 50  0001 C CNN
F 3 "" H 3650 6750 50  0001 C CNN
	1    3650 6750
	1    0    0    -1  
$EndComp
$Comp
L accent:VCC #PWR0111
U 1 1 5C98F086
P 3650 6600
F 0 "#PWR0111" H 3650 6450 50  0001 C CNN
F 1 "VCC" H 3667 6773 50  0000 C CNN
F 2 "" H 3650 6600 50  0000 C CNN
F 3 "" H 3650 6600 50  0000 C CNN
	1    3650 6600
	1    0    0    -1  
$EndComp
Text GLabel 3650 6900 3    50   Input ~ 0
SCL
$Comp
L accent:Hole H1
U 1 1 5C99B276
P 7200 6050
F 0 "H1" H 7478 6103 60  0000 L CNN
F 1 "Hole" H 7478 5997 60  0000 L CNN
F 2 "accent-footprint:Hole_M2" H 7200 6050 60  0001 C CNN
F 3 "" H 7200 6050 60  0000 C CNN
	1    7200 6050
	1    0    0    -1  
$EndComp
$Comp
L accent:Hole H2
U 1 1 5C99B385
P 8050 6050
F 0 "H2" H 8328 6103 60  0000 L CNN
F 1 "Hole" H 8328 5997 60  0000 L CNN
F 2 "accent-footprint:Hole_M2" H 8050 6050 60  0001 C CNN
F 3 "" H 8050 6050 60  0000 C CNN
	1    8050 6050
	1    0    0    -1  
$EndComp
$Comp
L accent:Hole H3
U 1 1 5C99B5AB
P 8900 6050
F 0 "H3" H 9178 6103 60  0000 L CNN
F 1 "Hole" H 9178 5997 60  0000 L CNN
F 2 "accent-footprint:Hole_M2" H 8900 6050 60  0001 C CNN
F 3 "" H 8900 6050 60  0000 C CNN
	1    8900 6050
	1    0    0    -1  
$EndComp
$Comp
L accent:Hole H5
U 1 1 5C99B8DF
P 10550 6050
F 0 "H5" H 10828 6103 60  0000 L CNN
F 1 "Hole" H 10828 5997 60  0000 L CNN
F 2 "accent-footprint:Hole_M2" H 10550 6050 60  0001 C CNN
F 3 "" H 10550 6050 60  0000 C CNN
	1    10550 6050
	1    0    0    -1  
$EndComp
$Comp
L accent:Hole H4
U 1 1 5C99D8E3
P 9750 6050
F 0 "H4" H 10028 6103 60  0000 L CNN
F 1 "Hole" H 10028 5997 60  0000 L CNN
F 2 "accent-footprint:Hole_M2" H 9750 6050 60  0001 C CNN
F 3 "" H 9750 6050 60  0000 C CNN
	1    9750 6050
	1    0    0    -1  
$EndComp
Text GLabel 1600 2100 0    50   Input ~ 0
SCL
Text GLabel 1600 2000 0    50   Input ~ 0
SDA
Text GLabel 1600 1700 0    50   Input ~ 0
DATA
Text GLabel 1600 1600 0    50   Input ~ 0
LED
Text GLabel 2900 1800 2    50   Input ~ 0
RESET
NoConn ~ 2900 2000
NoConn ~ 2900 1600
Wire Wire Line
	1600 1800 1600 1900
$Comp
L accent:GND #PWR0112
U 1 1 5C9ABC8B
P 1350 1900
F 0 "#PWR0112" H 1350 1650 50  0001 C CNN
F 1 "GND" V 1355 1772 50  0000 R CNN
F 2 "" H 1350 1900 50  0000 C CNN
F 3 "" H 1350 1900 50  0000 C CNN
	1    1350 1900
	0    1    1    0   
$EndComp
$Comp
L accent:GND #PWR0113
U 1 1 5C9ABD95
P 3200 1700
F 0 "#PWR0113" H 3200 1450 50  0001 C CNN
F 1 "GND" V 3205 1572 50  0000 R CNN
F 2 "" H 3200 1700 50  0000 C CNN
F 3 "" H 3200 1700 50  0000 C CNN
	1    3200 1700
	0    -1   -1   0   
$EndComp
$Comp
L accent:VCC #PWR0114
U 1 1 5C9ABEA6
P 3200 1900
F 0 "#PWR0114" H 3200 1750 50  0001 C CNN
F 1 "VCC" V 3217 2028 50  0000 L CNN
F 2 "" H 3200 1900 50  0000 C CNN
F 3 "" H 3200 1900 50  0000 C CNN
	1    3200 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 1900 2900 1900
Wire Wire Line
	3200 1700 2900 1700
Wire Wire Line
	1600 1900 1350 1900
Connection ~ 1600 1900
Text GLabel 2900 2700 2    50   Input ~ 0
col6
$Comp
L accent:PowerFlag #Junk0101
U 1 1 5C9B9886
P 4600 6550
F 0 "#Junk0101" H 4600 6550 50  0001 C CNN
F 1 "PowerFlag" H 4668 6594 50  0000 L CNN
F 2 "" H 4600 6550 50  0001 C CNN
F 3 "" H 4600 6550 50  0001 C CNN
	1    4600 6550
	1    0    0    -1  
$EndComp
$Comp
L accent:PowerFlag #Junk0102
U 1 1 5C9B9990
P 5300 6550
F 0 "#Junk0102" H 5300 6550 50  0001 C CNN
F 1 "PowerFlag" H 5368 6594 50  0000 L CNN
F 2 "" H 5300 6550 50  0001 C CNN
F 3 "" H 5300 6550 50  0001 C CNN
	1    5300 6550
	1    0    0    -1  
$EndComp
$Comp
L accent:GND #PWR0115
U 1 1 5C9B9B82
P 4600 6550
F 0 "#PWR0115" H 4600 6300 50  0001 C CNN
F 1 "GND" H 4605 6377 50  0000 C CNN
F 2 "" H 4600 6550 50  0000 C CNN
F 3 "" H 4600 6550 50  0000 C CNN
	1    4600 6550
	1    0    0    -1  
$EndComp
$Comp
L accent:VCC #PWR0116
U 1 1 5C9BBD66
P 5300 6550
F 0 "#PWR0116" H 5300 6400 50  0001 C CNN
F 1 "VCC" H 5318 6723 50  0000 C CNN
F 2 "" H 5300 6550 50  0000 C CNN
F 3 "" H 5300 6550 50  0000 C CNN
	1    5300 6550
	-1   0    0    1   
$EndComp
$Comp
L accent:Hole_mini HM1
U 1 1 5C995DB8
P 7200 5400
F 0 "HM1" H 7458 5453 60  0000 L CNN
F 1 "Hole_mini" H 7458 5347 60  0000 L CNN
F 2 "accent-footprint:Hole_M2_mini" H 7458 5294 60  0001 L CNN
F 3 "" H 7200 5400 60  0000 C CNN
	1    7200 5400
	1    0    0    -1  
$EndComp
$Comp
L accent:Hole_mini HM2
U 1 1 5C995EE6
P 8050 5400
F 0 "HM2" H 8308 5453 60  0000 L CNN
F 1 "Hole_mini" H 8308 5347 60  0000 L CNN
F 2 "accent-footprint:Hole_M2_mini" H 8308 5294 60  0001 L CNN
F 3 "" H 8050 5400 60  0000 C CNN
	1    8050 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 4250 10750 4250
$Comp
L accent:SW_PUSH SW25
U 1 1 5C98AF04
P 10450 3950
F 0 "SW25" H 10450 4295 50  0000 C CNN
F 1 "SW_PUSH" H 10450 4204 50  0000 C CNN
F 2 "accent-footprint:CherryMX_With_Socket" H 10450 4113 20  0000 C CNN
F 3 "" H 10450 3950 50  0000 C CNN
	1    10450 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:D D25
U 1 1 5C98AEFD
P 10750 4100
F 0 "D25" V 10796 4021 50  0000 R CNN
F 1 "D" V 10705 4021 50  0000 R CNN
F 2 "accent-footprint:Diode_SOD123" H 10750 4100 50  0001 C CNN
F 3 "~" H 10750 4100 50  0001 C CNN
	1    10750 4100
	0    -1   -1   0   
$EndComp
Connection ~ 9950 4250
$EndSCHEMATC
